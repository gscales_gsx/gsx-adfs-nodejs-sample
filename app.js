var express = require('express');
const https = require('https');
var fs = require('fs');
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser');
var jsonwebtoken = require('jsonwebtoken');

var passport = require('passport'),
    SamlStrategy = require('passport-saml').Strategy;

passport.serializeUser(function (user, done) {
    done(null, user);
});
passport.deserializeUser(function (user, done) {
    done(null, user);
});

passport.use(new SamlStrategy({
        entryPoint: 'https://dagwitness.gsxtest.com/adfs/ls',
        issuer: 'urn:microsoft:adfs:gsxtestadfs',
        callbackUrl: 'https://localhost/login/callback',
        cert: fs.readFileSync('ADFS-Signing-public.cer', 'utf-8'),
        authnContext: 'http://schemas.microsoft.com/ws/2008/06/identity/authenticationmethod/password',
        acceptedClockSkewMs: -1,
        identifierFormat: null,
        signatureAlgorithm: 'sha256',
        RACComparison: 'exact',
    },
    function (profile, done) {
        return done(null, {
            upn: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/upn'],
            groups: profile["http://schemas.microsoft.com/ws/2008/06/identity/claims/groupsid"],
            email: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'],
            displayName: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'],
            firstName: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname'],
            lastName: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname'],
        });
    }
));



var app = express();
const httpsServer = https.createServer({
    key: fs.readFileSync('servercert.key', 'utf8'),
    cert: fs.readFileSync('servercert.cer', 'utf8'),
}, app);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());

app.get('/',
    function (req, res) {
        res.send('Root of Web');
    });

app.get('/login',
    passport.authenticate('saml', {
        failureRedirect: '/',
        failureFlash: true
    }),
    function (req, res) {
        res.redirect('https://localhost');
    }
);


app.post('/login/callback',
    bodyParser.urlencoded({
        extended: false
    }),
    passport.authenticate('saml', {
        failureRedirect: '/',
        failureFlash: true
    }),
    //Check for Groups
    function (req, res) {
        console.log(req.user);
        if (Array.isArray(req.user.groups)) {
            if (req.user.groups.includes("GizmoAdmins")) {
                req.user["role"] = "Admin";
            } else {
                if (req.user.groups.includes("GizmoUsers")) {
                    req.user["role"] = "User";
                }
            }
        } else {
            switch (req.user.groups) {
                case "GizmoAdmins":
                    req.user["role"] = "Admin";
                    break;
                case "GizmoUsers":
                    req.user["role"] = "User";
                    break;
            }
        }
        if (req.user.hasOwnProperty("role")) {
            const signedToken = jsonwebtoken.sign(
                req.user,
                'shhhhh'
            );
            console.log(signedToken);
            res.json({
                message: "ok",
                role: req.user.role,
                token: signedToken
            });
        } else {
            res.json({
                message: "failed",
                error: "User not in any Gizmo Groups"
            });
        }

    }
);

httpsServer.listen(443, function () {
    console.log("Listening on 443");
});